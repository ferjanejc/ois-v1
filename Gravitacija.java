import java.util.*;
public class Gravitacija {
    public static void main (String []args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Vpisi nadmorsko visino: ");
        double visina = sc.nextDouble();
        System.out.println("Nadmorska visina: " + visina);
        System.out.println("Gravitacijski pospesek: " + pospesek(visina));
    }
    private static double pospesek(double visina) {
		double C = 6.674;
		double r = 6.371* Math.pow(10, 6);
		double M = 5.972;
		int koefC = 24;
		int koefM = -11;
		int koef = koefC + koefM;
		double vrniPospesek = C*M*Math.pow(10, koef) / Math.pow((r+visina), 2);
		return vrniPospesek;
	}
}